# The packaging information for websockets in XiVO

This repository contains the packaging information for
[websockets](https://websockets.readthedocs.org).

To get a new version of websockets in the XiVO repository, set the desired
version in the `VERSION` file and increment the changelog.

[Jenkins](http://jenkins.xivo.io) will then retrieve and build the new version.
